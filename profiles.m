% Generates surface brightness profiles of stars and object
% Computes object mangitude with error


function profiles

warning off

cwd=cd;
nlev=strfind(cwd,'/');
namedir=cwd(nlev(end)+1:end);

% Photometry parameters

magflux = 2^16;
errormag = 1;
kkmax = 20;
nL = 4;
ngap = 2;
nannulus = 3;
minL = 10;
mingap = 5;
minannulus = 6;

% Moffat function

ft_ = fittype('C/(1+x^2/R)^b','dependent',{'y'},'independent',{'x'}, ...
    'coefficients',{'C', 'R', 'b'});

facmaxS=1.075;

% List of images

fidl = fopen('list_images.csv');
lista = textscan(fidl,'%s%d%d','Delimiter',',');
fclose(fidl);
nimag = length(lista{1})

% File for results

fidr=fopen('result.txt','w');


% Loop over images

for ni = 1:nimag
    
    close all
    
    seeing = 2.5;
    L=ceil(nL*seeing);
    gap = ceil(ngap*seeing);
    annulus = ceil(nannulus*seeing);
    clear astname iniima finima u4
    astname = cell2mat(lista{1}(ni));
    
    iniima = num2str(lista{2}(ni),'%3.3d');
    finima = num2str(lista{3}(ni),'%3.3d');

    [astname ' ' iniima ' ' finima]
    
    % Data from astrometry.net
    eval(['m' astname '_' iniima '_' finima '_axy'])
    eval(['m' astname '_' iniima '_' finima '_rd'])
    eval(['m' astname '_' iniima '_' finima '_wcs'])

    % Data from UCAC4
    fidu = fopen(['m' astname '_' iniima '_' finima '_u4.tab']);
    u4 = textscan(fidu,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f','HeaderLines',9);
    fclose(fidu);
    RAu4 = u4{1};
    DECu4 = u4{2};
    magu4 = u4{3};

    % Read images alistar and aliobj
    nameimag_alistar = [astname '_alistar_sum_' iniima '-' finima '-imag'];
    nameimag_aliobj = [astname '_aliobj_sum_' iniima '-' finima '-imag'];
    image_alistar = fitsread([nameimag_alistar '.fits']);
    image_aliobj = fitsread([nameimag_aliobj '.fits']);

    % Remove saturated and close to edges stars
    selb = find(X < IMAGEW*0.03 | X > IMAGEW*0.97 | Y < IMAGEH*0.03 | Y > IMAGEH*0.97 | FLUX > 0.97*magflux);
    X(selb) = [];
    Y(selb) = [];
    RA(selb) = [];
    DEC(selb) = [];
    FLUX(selb) = [];
    BACKGROUND(selb) = [];
    nstar = length(RA);

    % Zeromag and seeing estimation

    k = 0;
    kk = 0;
    
    clear FLUXf magu4f seeingi
    for i = 1:15   
        [dmin,j]=min(sqrt(((RA(i)-RAu4)*cosd(CRVAL2)).^2+(DEC(i)-DECu4).^2));

        if dmin < 3/3600

            k = k+1;
            FLUXf(k) = FLUX(i);
            magu4f(k) = magu4(j);
            
            x = round(X(i));
            y = round(Y(i));
            Xe = X;
            Ye = Y;

            selfl = find(FLUX < FLUX(i)*0.10);
            selfl = [selfl ; i];
            Xe(selfl) = [];
            Ye(selfl) = [];
            [dminxy,jj] = min(sqrt((x-Xe).^2+(y-Ye).^2));
            maxI = max(max(image_alistar(y-L:y+L,x-L:x+L)));

            if (dminxy > L & maxI < 0.975*magflux) 
                x = round(X(i));
                y = round(Y(i));
                [centre0,skyval0,skynoise0,flag0] = centroid(image_alistar,x,y,L,gap,annulus);

                if flag0
                    kk = kk+1;
                    [centre,skyval,skynoise,flag] = centroid(image_alistar,centre0(1),centre0(2),L,gap,annulus);

                    x = round(centre(1));
                    y = round(centre(2));

                    clear r S

                    r = sqrt((ones(L*2+1,1)*[x-L:x+L]-centre(1)).^2+((ones(L*2+1,1)*[y-L:y+L])'-centre(2)).^2);

                    S = image_alistar(y-L:y+L,x-L:x+L) - skyval;
                    sel = find(r>L);
                    S(sel) = 0;
                    S = S(:);
                    r(S == 0) = [];
                    r = r';
                    S(S == 0) = [];
                    
                    if mean(S(end-10:end))>1e3                        
                        for t = length(S):-1:1
                            S(t) = [];
                        end
                    end
                    
                    ok_ = isfinite(r) & isfinite(S);
                    st_ = [max(S) 3 1 ];
                    stl_ = [max(S)*0.9 0 0 ];
                    stu_ = [max(S)*1.2 1000 100000 ];

                    % Moffat fit
                    
                    try                    
                        cf_ = fit(r(ok_),S(ok_),ft_,'Startpoint',st_,'Lower',stl_,'Upper',stu_);
                        seeingi(kk) = sqrt(cf_.R)*sqrt(2^(1/cf_.b)-1);
                    catch
                    end

                end
            end

        end
    end
    
    zeromag = median(-2.5*log10(FLUXf)-magu4f);
    
    seeing = median(seeingi);
    L = max(ceil(nL*seeing),minL);
    gap = max(ceil(ngap*seeing),mingap);
    annulus = max(ceil(nannulus*seeing),minannulus);
    
    rbin=[0.5:L-0.5];

    clear index FLUXf magu4f FLUXf2 magu4f2 skyvali
    rmt = [];
    St = [];
    k = 0;
    kk = 0;
    kkk = 0;
    
    % Loop over identified stars

    for i = 1:nstar

        [dmin,j] = min(sqrt(((RA(i)-RAu4)*cosd(CRVAL2)).^2+(DEC(i)-DECu4).^2));
        if dmin < 3/3600 & abs(-2.5*log10(FLUX(i))-magu4(j)-zeromag) < errormag
            
            k = k+1;
            index(k) = j;
            FLUXf(k) = FLUX(i);
            magu4f(k) = magu4(j);

            x = round(X(i));
            y = round(Y(i));
            Xe = X;
            Ye = Y;

	    % Remove weak stars
            selfl = find(FLUX < FLUX(i)*0.10);
            selfl = [selfl ; i];
            Xe(selfl) = [];
            Ye(selfl) = [];
            [dminxy,jj] = min(sqrt((x-Xe).^2+(y-Ye).^2));
            maxI = max(max(image_alistar(y-L:y+L,x-L:x+L)));

            if (dminxy > L & maxI < 0.975*magflux) 
                [centre0,skyval0,skynoise0,flag0] = centroid(image_alistar,x,y,L,gap,annulus);
                
                if flag0
                    kk = kk+1;
                    [centre,skyval,skynoise,flag] = centroid(image_alistar,centre0(1),centre0(2),L,gap,annulus);

                    skyvali(kk) = skyval;
                    skynoisei(kk) = skynoise;

                    x = round(centre(1));
                    y = round(centre(2));

                    clear r S

                    r = sqrt((ones(L*2+1,1)*[x-L:x+L]-centre(1)).^2+((ones(L*2+1,1)*[y-L:y+L])'-centre(2)).^2);

                    S = image_alistar(y-L:y+L,x-L:x+L) - skyval;
                    sel = find(r>L);
                    S(sel) = 0;
                    S = S(:);
                    r(S == 0) = [];
                    r = r';
                    S(S == 0) = [];                   

                    FLUXf2(kk) = sum(sum(S));
                    magu4f2(kk) = magu4(j);

                    % Moffat fit
                    if kkk < kkmax & FLUXf(k) > 0.005*magflux
                        ok_ = isfinite(r) & isfinite(S);
                        st_ = [max(S) 3 1 ];
                        stl_ = [max(S)*0.9 0 0 ];
                        stu_ = [max(S)*1.2 1000 100000 ];

                        try                            
                            cf_ = fit(r(ok_),S(ok_),ft_,'Startpoint',st_,'Lower',stl_,'Upper',stu_);
                            C = cf_.C;
                            R2 = cf_.R;
                            beta = cf_.b;

                            seeingk = sqrt(cf_.R)*sqrt(2^(1/cf_.b)-1);
                            if seeingk < seeing*1.25
                                kkk = kkk+1;

                                rmt = [rmt ; r];
                                St = [St ; S/C];
                            end
                        catch
                        end
                    end
                end
            end

        end
    end
    
    if kk>0
        skyvalm = median(skyvali);
        skynoisem = median(skynoisei);
    end

    if kkk > 0       
        
        ok_ = isfinite(rmt) & isfinite(St);
        st_ = [max(St) 3 1 ];
        cf_ = fit(rmt(ok_),St(ok_),ft_,'Startpoint',st_);

	% Compute errorbars
        
        dev_s = zeros(1,floor(max(rmt))+1);

        for h = 1:floor(max(rmt))+1
            n = length(St(find(rmt>=(h-1)&rmt<h)));
            d = sqrt((sum((St(find(rmt>=(h-1)&rmt<h)) - cf_(rmt(find(rmt>=(h-1)&rmt<h)))).^2))/n);
            dev_s(h)= d;
        end        
   
        rmt_dev = [0.5:1:floor(max(rmt))+0.5]';
        St_dev = cf_(rmt_dev);
        dev_s_low = dev_s';
        dev_s_upp = dev_s';        
        
        rmt_dev = rmt_dev - 0.06;  % Artifitial displacement for plots     
        
        magf=-2.5*log10(FLUXf);


        % Plot correlation between instrumental and catalog magnitude
        
        figure(2)
        plot(magu4f2,magf2,'.', 'MarkerSize',12)
        xlabel('Mag UCAC4','FontSize',14,'FontWeight','bold')
        ylabel('Mag Inst','FontSize',14,'FontWeight','bold')
        set(gca,'FontSize',14,'FontWeight','bold','LineWidth',2)
        hold on
        title([astname ' Date:' namedir ' - Images:' iniima '-' finima],'FontSize',14,'FontWeight','bold')

	% Magnitude adjustment
        axis equal
        ax = axis;
        zeromagf2 = median(magf2-magu4f2);
        plot([ax(1) ax(2)],[ax(1)+zeromagf2 ax(2)+zeromagf2],':k')
        hold off
        eval(['print -dpng -r300 ' astname '_' iniima '_' finima '_ajuste_foto.png'])
       
        
        % Object profile

        eval(['m' astname '_' iniima '_' finima '_obj_xy'])
        x = X(1);
        y = Y(1);
        
        [centre0,skyval0,skynoise0,flag0] = centroid(image_aliobj,x,y,L,gap,annulus);
        if flag0
            [centre,skyval,skynoise,flag] = centroid(image_aliobj,centre0(1),centre0(2),L,gap,annulus);
        else
            centre=[x y];
            skyval=skyvalm;
            skynoise=skynoisem;
        end
        
        x = round(centre(1));
        y = round(centre(2));

        r = sqrt((ones(L*2+1,1)*[x-L:x+L]-centre(1)).^2+((ones(L*2+1,1)*[y-L:y+L])'-centre(2)).^2);

        % Extract subimage
        subimage_aliobj=image_aliobj(y-10*L:y+10*L,x-10*L:x+10*L);
        subimage_aliobjp=image_aliobj(y-L:y+L,x-L:x+L); % subimagen mas pequen~a para fijar niveles de grises
        
	figure(3)
        imagesc(subimage_aliobj,[quantile(subimage_aliobjp(:),0.05) quantile(subimage_aliobjp(:),0.99)])
        axis square
        colormap gray
        set(gca,'FontSize',18,'FontWeight','bold','LineWidth',2)
        eval(['print -dpng -r300 ' astname '_' iniima '_' finima '_aliobj.png'])

        So = image_aliobj(y-L:y+L,x-L:x+L) - skyval;
        sel=find(r>L);
        So(sel)=0;
        So = So(:);
        r(So == 0) = [];
        r = r';
        So(So == 0) = [];

        if max(So) > 0.005*magflux & flag0

            ok_ = isfinite(r) & isfinite(So);
            st_ = [max(So) R2 1 ];
            stl_ = [max(So)*0.9 R2 0 ];
            stu_ = [max(So)*1.2 R2 100000 ];

            % Fit
            cfo_ = fit(r(ok_),So(ok_),ft_,'Startpoint',st_,'Lower',stl_,'Upper',stu_);   
                     
            C = cfo_.C;
            R2 = cfo_.R;
            beta = cfo_.b;            
        else
            C = max(So);
        end

        Sto = So(:)/C;

        % Std magnitude object
        FLUXo = sum(sum(So));
        mago = -2.5*log10(FLUXo);
        magu4o2 = mago-zeromagf2

        [p,S] = polyfit(magf2,magu4f2,1);
        [y_fit,delta] = polyval(p,magu4o2,S);       
       
        for p=length(magf2):-1:1
            if magf2(p) == inf
                magf2(p) = []
                magu4f2(p) = []
            end
        end
        
        delta_zero = sqrt(sum((magf2-magu4f2-zeromagf2).^2)/length(magf2))
        
        % SNR
        SNRo = FLUXo / skynoise / sqrt(numel(So))

        % Mag error
        deltamo = 2.5 * log10(1/SNRo + 1)        
        error_mag = sqrt(delta_zero^2+deltamo^2)
        
	% Superimposed profiles
        
        figure(1)        
        hh1 = plot(rmt,St,'b.','MarkerSize',12);
        hold on
        errorbar(rmt_dev,St_dev,dev_s_low,dev_s_upp,'.','Color',[1 0 0],'LineWidth',2,'MarkerSize',12) 
         
        h_ = plot(cf_,'fit',0.95);
        set(h_(1),'Color',[1 0 0],'LineStyle','-', 'LineWidth',2,'Marker', ...
            'none', 'MarkerSize',6);

        if max(So) > 0.005*magflux & flag0
            % Uso ajuste imponiendo maximo = 1
            
            warning off
            cfo_.C = 1;
            warning on
            
            clear dev
            
            dev = zeros(1,floor(max(r))+1);
            
            for h = 1:floor(max(r))+1
                n = length(Sto(find(r>=(h-1)&r<h)));
                d = sqrt((sum((Sto(find(r>=(h-1)&r<h)) - cfo_(r(find(r>=(h-1)&r<h)))).^2))/n);
                dev(h)= d;
            end
                        
            r_dev = [0.5:1:floor(max(r))+0.5]';
            Sto_dev = cfo_(r_dev);

            dev_low = dev';
            dev_upp = dev';        

            r_dev = r_dev + 0.06;               
           
            prom_dev = mean(dev(round(0.4*max(r)):round(0.8*max(r))));

	    % Profile quality
            
            if prom_dev < 0.05
                q = 1;
            elseif prom_dev < 0.1
                q = 2;
            else
                q = 3;
            end
            
            quality = q
            
            if max(r_dev) > 10
                xlim([0,max(r_dev)+0.5])
            end

            ho_ = plot(cfo_,'fit',0.95);
            set(ho_(1),'Color',[0 0 0],'LineStyle','-', 'LineWidth',3,'Marker', ...
                'none', 'MarkerSize',6);
            
            errorbar(r_dev,Sto_dev,dev_low,dev_upp,'.k','LineWidth',2,'MarkerSize',12)   
            
            legend([hh1 h_ hh2 ho_],{[num2str(kkk) ' Stars'],'Moffat fit to stars','Object','Moffat fit to object'})    
                            
        else
            % Mean values
            clear Sbin
            for ir = 1:length(rbin)
                Sbin(ir) = mean(Sto(find(abs(r-rbin(ir))<=0.5)));
            end           
            
            YY = spline(rbin,Sbin,r);            
            dev = zeros(1,floor(max(r))+1);

            for h = 1:floor(max(r))+1
                n = length(Sto(find(r>=(h-1)&r<h)));
                d = sqrt((sum((Sto(find(r>=(h-1)&r<h)) - YY(find(r>=(h-1)&r<h))).^2))/n);
                dev(h)= d;
            end
            
            r_dev = [0.5:1:floor(max(r))+0.5]';
            Sto_dev = Sbin;

            dev_low = dev';
            dev_upp = dev';      

            figure(1)
            hold on
            
            r_dev = r_dev+0.06;
            
            prom_dev = mean(dev(round(0.4*max(r)):round(0.8*max(r))));
            
            if prom_dev < 0.05
                q = 1;
            elseif prom_dev < 0.1
                q = 2;
            else
                q = 3;
            end
            
            quality = q
            
            hh2 = plot(r,Sto,'g.','MarkerSize',12) ;
            errorbar(r_dev,Sto_dev,dev_low,dev_upp,'k.','LineWidth',2,'MarkerSize',12) 
            ho_ = plot(rbin,Sbin,'Color',[0 0 0],'LineStyle','-', 'LineWidth',2);
            legend([hh1 h_ hh2 ho_],{[num2str(kkk) ' Stars'],'Moffat fit to stars','Object','Mean values of object'})    
        end
        
        ax = axis;
        axis([ax(1) ax(2) -0.1 1.1])
        xlabel('Distance (px)','FontSize',10,'FontWeight','bold')
        ylabel('Intensity','FontSize',10,'FontWeight','bold')
        set(gca,'FontSize',14,'FontWeight','bold','LineWidth',2)
        title([astname ' - ' namedir ' - #' iniima '-' finima  ' - V = ' num2str(magu4o2,'%6.2f')],'FontSize',15,'FontWeight','bold')
        hold off

        fprintf(fidr,'%s\n',[astname ' Date:' namedir ' - Images:' iniima '-' finima ' - seeing = ' num2str(seeing) ...
            ' - Cercano: ' num2str(not(flag0)) ' - Mag. obj. = ' num2str(magu4o2,'%6.3f') ' - Error_mag = ' num2str(error_mag,'%6.3f') ' - Quality = ' num2str(quality)]);

    end    
    
    eval(['print -dpng -r300 ' astname '_' iniima '_' finima '_profiles.png'])
    close all
        
end
fclose(fidr);

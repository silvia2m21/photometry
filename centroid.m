function [C,skyval,skynoise,flag] = centroid(imagefull,x,y,L,gap,annulus)

% flag = 1 - no close stars
%        0 - close stars

frac_skymax = 0.25;  % fraction of star brightess used for take flag=0 if there is another star
flag = 1;

x = round(x);
y = round(y);

% Sky backround
Lsky = L + gap + annulus;

% Exctract region of sky
skyima = imagefull(y-Lsky:y+Lsky,x-Lsky:x+Lsky);
rsky = sqrt((ones(Lsky*2+1,1)*[x-Lsky:x+Lsky]-x).^2+((ones(Lsky*2+1,1)*[y-Lsky:y+Lsky])'-y).^2);
selg=find(rsky>L+gap);
skymax=quantile(skyima(selg),0.97);

% Annulus pixels
sela=find(rsky<L+gap+annulus);
skyima(sela)=0;
skyima = skyima(:);
skyima(skyima == 0) = [];

skyval = median(skyima);
skynoise = std(skyima);
skymax=skymax-skyval;

% Object region
r = sqrt((ones(L*2+1,1)*[x-L:x+L]-x).^2+((ones(L*2+1,1)*[y-L:y+L])'-y).^2);
subimobj = imagefull(y-L:y+L,x-L:x+L) - skyval;

% Circle centered in the object
sel=find(r>L);
subimobj(sel)=0;

% Set flag = 0
if skymax > max(max(subimobj))*frac_skymax
    flag = 0;
end

% Compute centroid

I_total = sum(sum(subimobj));

I_x = sum(subimobj,1);
I_y = sum(subimobj,2)';

X = x-L:x+L;
Y = y-L:y+L;

x_centro = dot(X,I_x)/I_total;
y_centro = dot(Y,I_y)/I_total;

C = [x_centro,y_centro];
